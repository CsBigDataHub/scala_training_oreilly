class Foo(x:Int) {
   def apply(y:Int) = x + y
}

// When your methid is named 'apply' you can leave out the method name when you call it, demonstated in the below example


object MagicApply extends App {
  val foo = new Foo(10)
  println(foo.apply(20)) 
  println(foo(20)) 
}
