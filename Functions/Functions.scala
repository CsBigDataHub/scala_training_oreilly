object Functions extends App {
  //val f1:Function1[Int,Int] = new Functions[Int,Int]{
  //def apply(x:Int):Int = x+1
  //}
    //************ */
  //val f1:(Int => Int) = new Functions[Int,Int]{
  //def apply(x:Int):Int = x+1
  //}
  val f1 = (x: Int) => x + 1

//f0:Function0[Int]=new Function0[Int]{
//def apply()=1
//}
//************
//f0:(()=>Int)=new Function0[Int]{
//def apply()=1
//}

  val f0 = () => 1

  //val f2: Function2[Int, String, String] = new Function2[Int, String, String] {
  //  def apply(x: Int, y: String) = y + x
  //}
  //***********
  //val f2: (Int, String) => String = new Function2[Int, String, String] {
  //  def apply(x: Int, y: String) = y + x
  //}
  val f2 = (x: Int, y: String) => y + x

  println(f1(3))
  println(f0())
  println(f2(3, "Wow"))

  //val f3:String =>(String,Int) = (x: String) => (x, x.size)
  val f3 = (x: String) => (x, x.size)
  println(f3("Laser"))
}
