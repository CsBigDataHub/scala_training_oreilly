object FunctionsWithFunctions extends App {
  // val f:(Int,Int => Int) => Int = (x:Int, y:Int => Int) => y(x)
  val f = (x: Int, y: Int => Int) => y(x)
  println(f(3, (m: Int) => m + 2)) // here M is going to be 3 as in m=3
  println(f(3, m => m + 2)) // this works the same as above
  println(f(3, _ +2)) // this works the same as above
  println(f(3, 2 + _)) // this works the same as above
  println(f(3, 2 + )) // this works the same as above
  import scala.language.postfixOps
  println(f(3, 2 +))

  val g = (x: Int) => (y: Int) => x + y
  println(g(4)(5))
  println(g.apply(4).apply(5))
}
