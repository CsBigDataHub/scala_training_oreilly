object CurriedParameters extends App {
   def foo(x:Int, y:Int, z:Int) = x + y + z // when changed in to functions this gives (Int, Int, Int) => Int
   def bar(x:Int)(y:Int)(z:Int) = x + y + z// when changed in to functions this gives Int => (Int => (Int => Int))
   def baz(x:Int, y:Int)(z:Int) = x + y + z// when changed in to functions this gives (Int, Int) => Int => Int
   // we can change in to a function by val f =  foo _

   val f = foo(5, _:Int, _:Int)
   val g = bar(5) _
   val h = baz (_:Int,5) _
   
   println(f(4, 3))
   println(g(10)(19))
   println(g.apply(10).apply(19))
   println(h(4)(3))
}
