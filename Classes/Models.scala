import scala.beans.BeanProperty

//Creating abstract class
abstract class Person{
  def firstName:String
  def lastName:String
}

class Employee(@BeanProperty val firstName: String,
               @BeanProperty var lastName: String,
               val title: String) extends Person {
  require(firstName.nonEmpty, "FirstName cannot be empty")
  require(lastName.nonEmpty, "LastName cannot be empty")
  require(title.nonEmpty, "title cannot be empty")

  if (title.toLowerCase.contains("Senior".toLowerCase) || title.toLowerCase
        .contains("Junior".toLowerCase))
    throw new IllegalArgumentException("Title cannot be Junior or senoir")

  //A constructure typical form
  //def this(firstName: String, lastName: String) = this(firstName, lastName, "Programmer")
  // A typical form with sideeffects
//   def this(firstName: String, lastName: String) = {
//     this(firstName, lastName, "Programmer")
//     println("Side effects")
//   }
//// A typical form with out equals
  //def this(firstName: String, lastName: String) {
  //  this(firstName, lastName, "Programmer")
  //  println("Side effects")
  //}
  //// a different form of a constructure
  def this(firstName: String, lastName: String) = {
    this(firstName, lastName, "Programmer")
  }

  def fullName = s"$firstName $lastName"

  def changeLastName(ln: String) = new Employee(firstName, ln, title)

  def copy(firstName: String = this.firstName,
           lastName: String = this.lastName,
           title: String = this.title) = {
    new Employee(firstName, lastName, title)
  }

  override def equals(x: Any): Boolean = {
    if (!x.isInstanceOf[Employee]) false
    else {
      val other = x.asInstanceOf[Employee]
      // other.firstName.equals(this.firstName) &&
      // other.lastName.equals(this.lastName) &&
      // other.title.equals(this.title)
      other.firstName == this.firstName &&
      other.lastName == this.lastName &&
      other.title == this.title

    }
  }

  //overriding HashCode

  override def hashCode: Int = {
    var result = 19
    result = 31 * result + firstName.hashCode
    result = 31 * result + lastName.hashCode
    result = 31 * result + title.hashCode
    result

  }
  //overriding toString
  override def toString = s"Employee($firstName, $lastName, $title)"

}

//class Department(val name: String)
//this is for example of case class
case class Department(val name: String){
 //overriding toString menthod
 override def toString = s"Department: $name"
}
//case class Department(name: String) //do not need to declare val or var for arguments when using case class
//This is Over riding
class Manager(firstName: String,
              lastName: String,
              title: String,
              val department: Department)
    extends Employee(firstName, lastName, title) {
  override def fullName = s"$firstName $lastName,${department.name} Manager "

  override def copy(firstName: String = this.firstName,
                    lastName: String = this.lastName,
                    title: String = this.title) = {
    new Manager(firstName, lastName, title, department = new Department("Toys"))
  }
}
