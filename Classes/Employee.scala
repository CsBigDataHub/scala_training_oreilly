import scala.beans.BeanProperty

class Employee(@BeanProperty val firstName: String,
               @BeanProperty var lastName: String,
               val title: String) {
  require(firstName.nonEmpty, "FirstName cannot be empty")
  require(lastName.nonEmpty, "LastName cannot be empty")
  require(title.nonEmpty, "title cannot be empty")

  if (title.toLowerCase.contains("Senior".toLowerCase) || title.toLowerCase
        .contains("Junior".toLowerCase))
    throw new IllegalArgumentException("Title cannot be Junior or senoir")

  //A constructure typical form
  //def this(firstName: String, lastName: String) = this(firstName, lastName, "Programmer")
  // A typical form with sideeffects
//   def this(firstName: String, lastName: String) = {
//     this(firstName, lastName, "Programmer")
//     println("Side effects")
//   }
//// A typical form with out equals
  //def this(firstName: String, lastName: String) {
  //  this(firstName, lastName, "Programmer")
  //  println("Side effects")
  //}
  //// a different form of a constructure
  def this(firstName: String, lastName: String) = {
    this(firstName, lastName, "Programmer")
  }

  def fullName = s"$firstName $lastName"

  def changeLastName(ln: String) = new Employee(firstName, ln, lastName)

  def copy(firstName: String = this.firstName,
           lastName: String = this.lastName,
           title: String = this.title) = {
    new Employee(firstName, lastName, title)
  }

}

class Department(val name: String)

class Manager(firstName: String,
              lastName: String,
              title: String,
              val department: Department)
    extends Employee(firstName, lastName, title)
