object EmployeeScript extends App {

  val ada = new Employee("Ada", "Lovelace")
  println(ada.firstName)

  println("ada title " + ada.title)

  val newAda = ada.changeLastName("Byron,countess of Love lalace")
  //ada.lastName = "Byron,countess of Love lalace"
  println(newAda.lastName)
  println(newAda.firstName)
  println("Title " + newAda.title)

  val newestAda = ada.copy(lastName = "Byron,countess of Love lalace")

  val dennis = new Employee(lastName = "Ritchie", firstName = "Dennis")

  try {
    val bono = new Employee("bono", "", "singer")
  } catch {
    case iae: IllegalArgumentException => println(iae.getMessage)
  } finally {
    println("continue with the program")
  }

  try {
    val bono = new Employee("Linus", "Torvalds", "senior C Program")
  } catch {
    case iae: IllegalArgumentException => println(iae.getMessage)
  } finally {
    println("continue with the program")
  }

  println(s"The first name is ${dennis.firstName}")
  println(s"The last name is ${dennis.lastName}")

  try {
    val testNull = new Employee("test", "", "")
  } catch {
    case iae: IllegalArgumentException => println(iae.getMessage)
  } finally {
    println("continue with the program")
  }
  println("***********")

  val maths = new Department("Maths")
  val alan: Manager = new Manager("Alan", "Turing", "Mathematician", maths)
  println("alan's department Name " + alan.department.name)
  println("alan's title " + alan.title)
  println("*********")

  val alanEmployee: Employee = alan

  println(alanEmployee.firstName)

  def extractFirstName(e: Employee) = e.firstName
  println(extractFirstName(ada))
  println(extractFirstName(dennis))
  println(extractFirstName(alan))

  println(newestAda.fullName)
  println(newAda.fullName)
  println(dennis.fullName)
  println(ada.fullName)

  println("*************")
  println(alan.fullName)
  println(alanEmployee.fullName)

  val alanNewJob = alan.copy(title = "Encryption sepcialist")
  println(alanNewJob.title)
  println(alanNewJob.department.name)

  val anotherAda =
    new Employee("Ada", "Byron,countess of Love lalace", "Programmer")

  println(newAda.equals(anotherAda)) // Eqauals method overridden
  println(newAda == anotherAda) // We can also use '==' instead of overriding the method
  println(newAda != anotherAda)
  println(newAda eq anotherAda) // this 'eq' will test for reference equality, meaning that weather the objects are pointing to the same reference

  val evenAnotherAda = anotherAda
  // demoing refence eqality
  println(evenAnotherAda eq anotherAda)

  println(newAda.hashCode == anotherAda.hashCode)
  println(ada.hashCode != anotherAda.hashCode)
  println(newAda.toString)

//Implementing case class
  val toys = new Department("Toys")
  println(toys.toString)

  val toys2 = Department("Toys") // do not need to use 'new' keyword when creating a object
  println(toys == toys2)
  println(toys.hashCode == toys2.hashCode)
  val hardware = toys.copy(name = "Hardware")
  println(hardware)

//pattern matching

  val name = toys match {
    case Department(n) => n
    case _            => "unknown"
  }
  println(name)

//Simplyfying pattern matching
 val Department(name2)=toys
 println(name2)

 //Implementing Polymorphisam or abstract

 val alanPerson:Person = alan

 println(alanPerson.firstName)
 println(alanPerson.lastName)

}
