case class Box[T](t:T) {
  //'T' referes to AnyType
  //A parameterised type needs to be in [], so it can be anything in this case 'T' ot 'U'
  def coupleWith[U](u:U):Box[Couple[T, U]] = Box(Couple(t, u))
}
