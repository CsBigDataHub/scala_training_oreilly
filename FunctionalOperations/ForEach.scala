object ForEach extends App {
  val a = 1 to 10
  //println(a.map(x=>println(x)))

  //println(a.foreach(x=>println(x)))
  a.foreach(x=>println(x))
  // the blow is the cleaned up version of above
  a foreach println
}
